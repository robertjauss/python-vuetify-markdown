# Change Log

## 0.2.1 (2024/05/15)

**Bugfixes:**

- Changed order of operations, apparently that matters to markdown
- Removed unneeded debug code
- Added error handling to catch empty tags and return as they were entered

## 0.2.0 (2024/05/14)

**Implemented enhancements:**

- Modified code to work with newer version of Markdown
- Added several new styles for various text decorations
- Added spoiler text. A basic implementation, but works well enough
- Minor version bump due to breaking changes between older Markdown version and new

## 0.1.4 (2020/08/24)

**Implemented enhancements:**

- Modified output classes to match new Vuetify syntax

## 0.1.3 (2020/01/08)

**Implemented enhancements:**

- Moved code from it's own file to the init to reduce import complexity
- Added pip installation instructions to README

## 0.1.2 (2020/01/08)

**Implemented enhancements:**

- Initial repo creation and upload
- Support for H1-H6 tags, P tags